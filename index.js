const express = require('express');
var cors = require('cors')
const nocache = require('nocache');

const app = express();

app.options('*', cors()) // include before other routes
// C R U D
app.use(express.urlencoded({extended: true}));
app.use(express.json());
//app.use('/todos/:id',logger);
const todosRoutes = require('./routes/todos');
const listsRoutes = require('./routes/lists');
app.set('etag', false);
app.use(nocache());
app.disable('view cache');
express.static( '*', nocache());
express.static('*', {etag: false})


app.use('/private/todos', todosRoutes);
app.use('/private/lists', listsRoutes);

app.listen(process.env.PORT || 3000, () => console.log('listening on port 3000'));
